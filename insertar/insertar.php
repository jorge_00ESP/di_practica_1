<?php require_once('../sesion/seguridad.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style type="text/css">
         .volver{ 
            float: right;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <a href="insertar.php" class="navbar-brand"> INSERTAR PHP</a>
        </div>
    </nav>
    <section>
        <div class="container">
            <button onclick="location.href='../DI_practica.php'" class="btn btn-primary volver">Volver</button>
            <h2>Insertar Alumno</h2>
            <form class="form-horizontal" action="insertar.php" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="dni">DNI</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="dni" name="dni" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nombre">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="apellido_1">Apellido 1</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="apellido_1" name="apellido_1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="apellido_2">Apellido 2</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="apellido_2" name="apellido_2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="direccion">Direccion</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="direccion" name="direccion">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="localidad">Localidad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="localidad" name="localidad">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="provincia">Provincia</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="provincia" name="provincia">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nacimiento">Nacimiento</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nacimiento" name="nacimiento">
                    </div>
                </div>
                <input type="submit" value="Enviar" name="submit" class="btn btn-primary">
            </form>
        </div>
    </section>
        <?php
            $con = new PDO('mysql:host=localhost; dbname=universidad', 'root', '');
            $con-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt="";
            $consulta="INSERT INTO alumno VALUES (";
            $parametros=array();

            if(isset($_POST['submit'])){
                $dni=$_POST['dni'];
                $nombre = $_POST['nombre'];
                $apellido_1 = $_POST['apellido_1'];
                $apellido_2 = $_POST['apellido_2'];
                $direccion = $_POST['direccion'];
                $localidad = $_POST['localidad'];
                $provincia = $_POST['provincia'];
                $nacimiento = $_POST['nacimiento'];
            }

            if(isset($_POST['dni']) && !empty($_POST['dni'])){
                $consulta.=":dni";
                $parametros[":dni"]=$dni;
            }
            if(isset($_POST['apellido_1']) && !empty($_POST['apellido_1'])){
                $consulta.=", :apellido1";
                $parametros[":apellido1"]=$apellido_1;
            }else{
                $consulta.=", null";
            }

            if(isset($_POST['apellido_2']) && !empty($_POST['apellido_2'])){
                $consulta.=", :apellido2";
                $parametros[":apellido2"]=$apellido_2;
            }else{
                $consulta.=", null";
            }

            if(isset($_POST['nombre']) && !empty($_POST['nombre'])){
                $consulta.=", :nombre";
                $parametros[":nombre"]=$nombre;
            }else{
                $consulta.=", null";
            }

            if(isset($_POST['localidad']) && !empty($_POST['localidad'])){
                $consulta.=", :localidad";
                $parametros[":localidad"]=$localidad;
            }else{
                $consulta.=", null";
            }

            if(isset($_POST['direccion']) && !empty($_POST['direccion'])){
                $consulta.=", :direccion";
                $parametros[":direccion"]=$direccion;
            }else{
                $consulta.=", null";
            }

            if(isset($_POST['provincia']) && !empty($_POST['provincia'])){
                $consulta.=", :provincia";
                $parametros[":provincia"]=$provincia;
            }else{
                $consulta.=", null";
            }

            if(isset($_POST['nacimiento']) && !empty($_POST['nacimiento'])){
                $consulta.=", :nacimiento)";
                $parametros[":nacimiento"]=$nacimiento;
            }else{
                $consulta.=", null)";
            }

            try{
                if(isset($_POST['submit'])){
                    $stmt=$con->prepare($consulta);
                    $stmt->execute($parametros);
                }
    
                echo "<br>";
                if($stmt==true){
                    echo "Alumno insertado correctamente";
                }
                
            }catch(Exception $e){
                echo 'No se ha podido insertar el Alumno ', $e->getMessage();
            }
        ?>
    </section>
</body>
</html>