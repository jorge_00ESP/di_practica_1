<?php

require_once('../sesion/seguridad.php');

$con = new PDO('mysql:host=localhost; dbname=universidad', 'root', '');
$con-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$delete="DELETE FROM alumno WHERE true";
$parametros=array();

try{
    if(isset($_GET['dni'])){
        $parametros[':dni']=$_GET['dni'];
        $delete.=" and DNI=:dni";

        $stmt_d=$con->prepare($delete);
        $stmt_d->execute($parametros);
    }

    header("Location: ../DI_practica.php");
    die();
    
}catch(Exception $e){
    echo 'No se ha podido borrar el Alumno ', $e->getMessage();
    
    header("Location: ../DI_practica.php");
}
?>