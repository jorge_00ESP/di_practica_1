<?php

require_once('../sesion/seguridad.php');

$con = new PDO('mysql:host=localhost; dbname=universidad', 'root', '');
$con-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/809a7a252a.js" crossorigin="anonymous"></script>
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <a href="borrar.php" class="navbar-brand"> BORRAR PHP</a>
        </div>
    </nav>
    <?php

    $delete="DELETE FROM alumno WHERE true";
    $parametros=array();

    try{
        if(isset($_GET['dni'])){
            $parametros[':dni']=$_GET['dni'];
            $delete.=" and DNI=:dni";

            $stmt_d=$con->prepare($delete);
            $stmt_d->execute($parametros);
        }

        if($stmt_d==true){
            echo "El alumno con DNI ".$_GET['dni']." se ha borrado correctamente.";
        }else{
            echo "El alumno con DNI ".$_GET['dni']." no se ha borrado debido a un error.";
        }
        
    }catch(Exception $e){
        echo 'No se ha podido borrar el Alumno ', $e->getMessage();
    }

    ?>
    <section>
        <br><button onclick="location.href='../DI_practica.php'" class="btn btn-dark">Volver</button>
    </section>
</body>