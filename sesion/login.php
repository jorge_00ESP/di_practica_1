<?php
session_start();

/* Si ya he iniciado sesion, si me redirijo al login por la url, que me lleve al index,
ya que no necesito iniciar sesion otra vez

if(isset($_SESSION['email']) && isset($_SESSION['pass'])){
  header('Location: http://localhost/acceso_datos/DI_practica.php');
}
*/
$con = new PDO('mysql:host=localhost; dbname=universidad', 'root', '');
$con-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$mensaje="";

try{
    if(isset($_POST['login'])){
        if(empty($_POST['email'])){
            $mensaje= "<label> Se requieren todos los campos </label>";
        }else{
            $consulta='SELECT * FROM sesion WHERE email=:email AND pass=:pass';
            $stmt=$con->prepare($consulta);
            $parametros=array('email' => $_POST['email'], 'pass' => $_POST['pass']);
            $stmt->execute($parametros);
    
            $count=$stmt->rowCount();
            
            if($count==1){
                $_SESSION['email']=$_POST['email'];
                header("location: ../DI_practica.php");
        
            }else{
                $mensaje="<label> E-mail o contraseña incorrectos </label>";
            }
        }
    }
}catch(Exception $e){
    echo 'Error con la conexion del LOGIN', $e->getMessage();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/809a7a252a.js" crossorigin="anonymous"></script>
    <!--Login-->
    <link href="css/login.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="http://danielzawadzki.com/codepen/01/icon.svg" id="icon" alt="Practica PHP" />
    </div>

    <!-- Login Form -->
    <form method="post" action="login.php">
      <input type="text" id="login" class="fadeIn second" name="email" placeholder="E-mail">
      <input type="password" id="password" class="fadeIn third" name="pass" placeholder="Contraseña">
      <input type="submit" class="fadeIn fourth" value="Iniciar Sesion" name="login">
    </form>

    <!-- Remind Passowrd 
    <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div>

  </div>
  -->
  <?php
    if(isset($mensaje)){
        echo '<label class="text-danger">'.$mensaje.'</label>';
    }
  ?>
</div>
</body>
</html>