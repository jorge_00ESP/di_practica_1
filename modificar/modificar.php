<?php 

require_once('../sesion/seguridad.php');

if(isset($_POST['submit'])){
    $dni=$_POST['dni'];
}else{
    $dni=$_GET['dni'];
}

$con = new PDO('mysql:host=localhost; dbname=universidad', 'root', '');
$con-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$consulta='SELECT * FROM alumno WHERE DNI='.$dni;
$stmt=$con->prepare($consulta);
$stmt->execute();
$fetch_consulta=$stmt->fetch(PDO::FETCH_ASSOC);

if(isset($_POST['submit'])){
    $nombre=$_POST['nombre'];
    $apellido_1=$_POST['apellido_1'];
    $apellido_2=$_POST['apellido_2'];
    $direccion=$_POST['direccion'];
    $localidad=$_POST['localidad'];
    $provincia=$_POST['provincia'];
    $nacimiento=$_POST['nacimiento'];

}else{
    $nombre=$fetch_consulta['NOMBRE'];
    $apellido_1=$fetch_consulta["APELLIDO_1"];
    $apellido_2=$fetch_consulta["APELLIDO_2"];
    $direccion=$fetch_consulta["DIRECCION"];
    $localidad=$fetch_consulta["LOCALIDAD"];
    $provincia=$fetch_consulta["PROVINCIA"];
    $nacimiento=$fetch_consulta["FECHA_NACIMIENTO"];
}

$update="UPDATE alumno SET";
$parametros=array();

if(isset($_POST['dni']) && !empty($_POST['dni'])){
    $parametros[":dni"]=" DNI=".$dni;
}

if(isset($_POST['apellido_1']) && !empty($_POST['apellido_1'])){
    $parametros[":apellido1"]=" APELLIDO_1='".$apellido_1."'";
}

if(isset($_POST['apellido_2']) && !empty($_POST['apellido_2'])){
    $parametros[":apellido2"]=" APELLIDO_2='".$apellido_2."'";
}

if(isset($_POST['nombre']) && !empty($_POST['nombre'])){
    $parametros[":nombre"]=" NOMBRE='".$nombre."'";
}

if(isset($_POST['localidad']) && !empty($_POST['localidad'])){
    $parametros[":localidad"]=" LOCALIDAD='".$localidad."'";
}

if(isset($_POST['direccion']) && !empty($_POST['direccion'])){
    $parametros[":direccion"]=" DIRECCION='".$direccion."'";
}

if(isset($_POST['provincia']) && !empty($_POST['provincia'])){
    $parametros[":provincia"]=" PROVINCIA='".$provincia."'";
}

if(isset($_POST['nacimiento']) && !empty($_POST['nacimiento'])){
    $parametros[":nacimiento"]=" FECHA_NACIMIENTO='".$nacimiento."'";
}

$comas=implode(",", $parametros );
$update.=$comas;
$update.=" WHERE DNI=".$dni;


if(isset($_POST['submit'])){
    $stmt_up=$con->prepare($update);
    $stmt_up->execute();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style type="text/css">
         .volver{ 
            float: right;
        }
    </style>
    <title>Document</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <a href="Modificar.php" class="navbar-brand"> MODIFICAR PHP</a>
        </div>
    </nav>
    <section>
        <div class="container">
            <h2>Modificar Alumno</h2>
            <button onclick="location.href='../DI_practica.php'" class="btn btn-primary volver">Volver</button>
            <form class="form-horizontal" action="modificar.php" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="dni">DNI</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="dni" name="dni" value="<?php echo $dni; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nombre">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="apellido_1">Apellido 1</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="apellido_1" name="apellido_1" value="<?php echo $apellido_1; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="apellido_2">Apellido 2</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="apellido_2" name="apellido_2" value="<?php echo $apellido_2; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="direccion">Direccion</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $direccion; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="localidad">Localidad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="localidad" name="localidad" value="<?php echo $localidad; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="provincia">Provincia</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="provincia" name="provincia" value="<?php echo $provincia; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nacimiento">Nacimiento</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nacimiento" name="nacimiento" value="<?php echo $nacimiento; ?>">
                    </div>
                </div>
                <input type="submit" value="Enviar" name="submit" class="btn btn-primary">
            </form>
            <?php
            if(isset($stmt_up)){
                if($stmt_up==true){
                    echo "Alumno modificado correctamente";
                }
            }
            ?>
        </div>
    </section>
</body>
</html>