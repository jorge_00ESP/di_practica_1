<?php

require_once('sesion/seguridad.php');

if(isset($_POST['submit'])){
    $dni=$_POST['dni'];
    $nombre = $_POST['nombre'];
    $apellido_1 = $_POST['apellido_1'];
    $apellido_2 = $_POST['apellido_2'];
    $direccion = $_POST['direccion'];
    $localidad = $_POST['localidad'];
    $provincia = $_POST['provincia'];
    $nacimiento = $_POST['nacimiento'];
    $view=$_POST['view'];
}else{
    $view=10;
}

//Si el GET pagina esta vacio, se le asigna el valor 1 por defecto
$pagina=isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/809a7a252a.js" crossorigin="anonymous"></script>
    <style type="text/css">
         .enviar{ 
            float: right;
        }

        .sesion{
            float: right;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <a href="DI_practica.php" class="navbar-brand"> PRACTICA PHP</a>
        </div>
    </nav>
    <nav>
        <form action="DI_practica.php" method="post" class="form-inline">
            <div class="form-group">
                <label for="dni">DNI</label>
                <input type="text" id="dni" name="dni" class="form-control">
            </div>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" id="nombre" name="nombre" class="form-control">
            </div>
            <div class="form-group">
                <label for="apellido_1">Apellido 1</label>
                <input type="text" id="apellido_1" name="apellido_1" class="form-control">
            </div>
            <div class="form-group">
                <label for="apellido_2">Apellido_2</label>
                <input type="text" id="apellido_2" name="apellido_2" class="form-control">
            </div>
            <div class="form-group">
                <label for="direccion">Direccion</label>
                <input type="text" id="direccion" name="direccion" class="form-control">
            </div>
            <div class="form-group">
                <label for="localidad">Localidad</label>
                <input type="text" id="localidad" name="localidad" class="form-control">
            </div>
            <div class="form-group">
                <label for="provincia">Provincia</label>
                <input type="text" id="provincia" name="provincia" class="form-control">
            </div>
            <div class="form-group">
                <label for="nacimiento">Nacimiento</label>
                <input type="text" id="naciminacimiento" name="nacimiento" class="form-control">
            </div>
            <div class="form-group">
            <label for="view">Limitar</label>
                <select id="view" name="view" class="custom-select" >
                    <option <?php echo($view ==5 ? "selected" : "") ?> value="5">5</option>
                    <option <?php echo($view ==10 ? "selected" : "") ?> value="10">10</option>
                    <option <?php echo($view ==15 ? "selected" : "") ?> value="15">15</option>
                    <option <?php echo($view ==20 ? "selected" : "") ?> value="20">20</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" value="Enviar" name="submit" class="btn btn-primary enviar">
            </div>
        </form><br>
        <button onclick="location.href='insertar/insertar.php'" class="btn btn-dark">Insertar alumno</button>
        <button onclick="location.href='sesion/logout.php'" class="btn btn-danger sesion">Cerrar Sesion</button>
    </nav><br>
    <section>
        <table class="table table-hover">
            <thead>
                <tr>
                    <td scope="col"><b>DNI</b></td>
                    <td scope="col"><b>Nombre</b></td>
                    <td scope="col"><b>Apellido 1</b></td>
                    <td scope="col"><b>Apellido 2</b></td>
                    <td scope="col"><b>Direccion</b></td>
                    <td scope="col"><b>Localidad</b></td>
                    <td scope="col"><b>Provincia</b></td>
                    <td scope="col"><b>Nacimiento</b></td>
                    <td scope="col"><b>Acciones</b></td>
                </tr>
            </thead>
        <?php
        $con = new PDO('mysql:host=localhost; dbname=universidad', 'root', '');
        $con-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $consulta='SELECT * FROM alumno WHERE true';
        $parametros=array();
                    
                    
        if(isset($_POST['dni']) && !empty($_POST['dni'])){
            $consulta.=" and DNI=:dni ";
                    $parametros[":dni"]=$dni;
        }
                        
        if(isset($_POST['nombre']) && !empty($_POST['nombre'])){
            $consulta.=" and NOMBRE like :nombre ";
            $parametros[":nombre"]="%".$nombre."%";
        }

        if(isset($_POST['apellido_1']) && !empty($_POST['apellido_1'])){
            $consulta.=" and APELLIDO_1 like :apellido1 ";
            $parametros[":apellido1"]="%".$apellido_1."%";
        }

        if(isset($_POST['apellido_2']) && !empty($_POST['apellido_2'])){
            $consulta.=" and APELLIDO_2 like :apellido2 ";
            $parametros[":apellido2"]="%".$apellido_2."%";
        }

        if(isset($_POST['localidad']) && !empty($_POST['localidad'])){
            $consulta.=" and LOCALIDAD like :localidad ";
            $parametros[":localidad"]="%".$localidad."%";
        }

        if(isset($_POST['direccion']) && !empty($_POST['direccion'])){
            $consulta.=" and DIRECCION like :direccion ";
            $parametros[":direccion"]="%".$direccion."%";
        }

        if(isset($_POST['provincia']) && !empty($_POST['provincia'])){
            $consulta.=" and PROVINCIA like :provincia ";
            $parametros[":provincia"]="%".$provincia."%";
        }

        if(isset($_POST['nacimiento']) && !empty($_POST['nacimiento'])){
            $consulta.=" and FECHA_NACIMIENTO=:nacimiento ";
            $parametros[":nacimiento"]=$nacimiento;
        }
                     
        //Inicio son los registros que se muestran en pantalla
        $inicio=($pagina>1) ? (($pagina * $view)-$view) : 0;

        $limit=" LIMIT $inicio, $view";

        $stmt=$con->prepare($consulta.$limit);
        $stmt->execute($parametros);
        $fetch_consulta=$stmt->fetchAll(PDO::FETCH_ASSOC);

        //Total de los registros y del numero de paginas
        $stmt_count=$con->prepare($consulta);
        $stmt_count->execute($parametros);
        $t_registros=$stmt_count->rowCount();

        $n_paginas=ceil($t_registros/$view);
        
        ?>
        
            <tbody>
            <?php foreach($fetch_consulta as $datos):?>
                <tr>
                    <td scope="row"><?php echo $datos["DNI"] ?></td>
                    <td><?php echo $datos["NOMBRE"] ?></td>
                    <td><?php echo $datos["APELLIDO_1"] ?></td>
                    <td><?php echo $datos["APELLIDO_2"] ?></td>
                    <td><?php echo $datos["DIRECCION"] ?></td>
                    <td><?php echo $datos["LOCALIDAD"] ?></td>
                    <td><?php echo $datos["PROVINCIA"] ?></td>
                    <td><?php echo $datos["FECHA_NACIMIENTO"] ?></td>
                    <td>
                        <a href="modificar/modificar.php?dni=<?php echo $datos['DNI']; ?>"><i class="fas fa-pen-alt"></i></a>
                        <a onclick="confirm('¿Estas seguro que quieres borrar este alumno?')"  href="borrar/borrar.php?dni=<?php echo $datos["DNI"]; ?>"><i class="fas fa-trash-alt"></i></a>
                    </td>
                </tr>                        
            <?php endforeach?>
            </tbody>
        </table>
    </section>
    <!--Paginador-->
    <section>
        <!--Esqueleto de la paginacion-->
        <nav aria-label="Page navigation example">
            <ul class="pagination">
            <!--Anterior-->
                <li class="page-item <?php echo $_GET['pagina']<=1 ? 'disabled' : '' ?>">
                    <a class="page-link" 
                    href="DI_practica.php?pagina=<?php echo $_GET['pagina']-1?>"
                    aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <!--Numeros-->
                <?php for($i=0;$i<$n_paginas;$i++): ?>
                    <li class="page-item <?php echo $_GET['pagina']==$i+1 ? 'active' : '' ?>">
                        <a class="page-link" href="DI_practica.php?pagina=<?php echo $i+1; ?>">
                        <?php echo $i+1; ?>
                        </a>
                    </li>
                <?php endfor ?>
                <!--Siguiente-->
                <li class="page-item <?php echo $_GET['pagina']>=$n_paginas ? 'disabled' : '' ?>">
                <a class="page-link" 
                href="DI_practica.php?pagina=<?php echo $_GET['pagina']+1?>" 
                aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
                </li>
            </ul>
        </nav>
        <!--Fin del esqueleto-->
        <?php
            
            echo "<b> Numero de registros: ".$t_registros."</b>";
            echo "<b> Pagina ".$pagina."/".$n_paginas."</b>";
            
        ?>
    </section>
</body>
</html>